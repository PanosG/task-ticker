package rest

import (
	"encoding/json"
	"log"
	"net/http"
)

var paramsErrorResponse = map[string]string{"period": "parameter `period` missing",
	"tz": "parameter `tz - Timezone` missing",
	"t1": "parameter `t1 - Start date` missing",
	"t2": "parameter `t2 - End date` missing",
}

func (s *Server) RegisterRouterEndpoints() *http.ServeMux {

	mux := http.NewServeMux()

	mux.Handle("/ptlist", requestParameterInterceptor(http.HandlerFunc(s.interval)))

	return mux
}

func requestParameterInterceptor(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := r.URL.Query()
		for k, v := range paramsErrorResponse {
			if !verifyParameter(params.Get(k) != "", v, w) {
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}

func sendResponse(status int, body []byte, w http.ResponseWriter) {
	w.WriteHeader(status)
	_, err := w.Write(body)
	if err != nil {
		log.Printf("error sending response %s\n", err.Error())
	}
}

func sendErrorResponse(status, description string, w http.ResponseWriter) {
	e := errorResponse{
		Status:      status,
		Description: description,
	}
	response, err := json.Marshal(e)
	if err != nil {
		log.Printf("error serializing resposne %v, %s \n", e, err.Error())
		sendResponse(http.StatusBadRequest, nil, w)
		return
	}
	sendResponse(http.StatusBadRequest, response, w)
}

func verifyParameter(condition bool, errorBody string, w http.ResponseWriter) bool {
	if !condition {
		sendErrorResponse("error", errorBody, w)
	}
	return condition
}
