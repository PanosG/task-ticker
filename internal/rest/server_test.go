package rest

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"task_ticker/internal/core/service"
)

var ts *httptest.Server

func TestMain(m *testing.M) {
	srv := &Server{Port: 9000, svc: &service.TickerService{}}
	ts = httptest.NewServer(srv.RegisterRouterEndpoints())
	m.Run()
}

type response struct {
	status int
	body   []string
}

func TestExampleUrls(t *testing.T) {
	tests := []struct {
		name string
		url  string
		want response
	}{
		{
			name: "Hourly Example",
			url:  "/ptlist?period=1h&tz=Europe/Athens&t1=20210714T204603Z&t2=20210715T123456Z",
			want: response{
				status: http.StatusOK,
				body: []string{"20210714T210000Z",
					"20210714T220000Z",
					"20210714T230000Z",
					"20210715T000000Z",
					"20210715T010000Z",
					"20210715T020000Z",
					"20210715T030000Z",
					"20210715T040000Z",
					"20210715T050000Z",
					"20210715T060000Z",
					"20210715T070000Z",
					"20210715T080000Z",
					"20210715T090000Z",
					"20210715T100000Z",
					"20210715T110000Z",
					"20210715T120000Z"},
			},
		},
		{
			name: "Daily Example",
			url:  "/ptlist?period=1d&tz=Europe/Athens&t1=20211010T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusOK,
				body: []string{"20211010T210000Z",
					"20211011T210000Z",
					"20211012T210000Z",
					"20211013T210000Z",
					"20211014T210000Z",
					"20211015T210000Z",
					"20211016T210000Z",
					"20211017T210000Z",
					"20211018T210000Z",
					"20211019T210000Z",
					"20211020T210000Z",
					"20211021T210000Z",
					"20211022T210000Z",
					"20211023T210000Z",
					"20211024T210000Z",
					"20211025T210000Z",
					"20211026T210000Z",
					"20211027T210000Z",
					"20211028T210000Z",
					"20211029T210000Z",
					"20211030T210000Z",
					"20211031T220000Z",
					"20211101T220000Z",
					"20211102T220000Z",
					"20211103T220000Z",
					"20211104T220000Z",
					"20211105T220000Z",
					"20211106T220000Z",
					"20211107T220000Z",
					"20211108T220000Z",
					"20211109T220000Z",
					"20211110T220000Z",
					"20211111T220000Z",
					"20211112T220000Z",
					"20211113T220000Z",
					"20211114T220000Z"},
			},
		},
		{
			name: "Monthly Example",
			url:  "/ptlist?period=1mo&tz=Europe/Athens&t1=20210214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusOK,
				body: []string{"20210228T220000Z",
					"20210331T210000Z",
					"20210430T210000Z",
					"20210531T210000Z",
					"20210630T210000Z",
					"20210731T210000Z",
					"20210831T210000Z",
					"20210930T210000Z",
					"20211031T220000Z"},
			},
		},
		{
			name: "Yearly Example",
			url:  "/ptlist?period=1y&tz=Europe/Athens&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusOK,
				body: []string{
					"20181231T220000Z",
					"20191231T220000Z",
					"20201231T220000Z",
				},
			},
		},
		{
			name: "Unsupported Period",
			url:  "/ptlist?period=1w&tz=Europe/Athens&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"unsupported period",
				},
			},
		},
		{
			name: "Invalid timestamp",
			url:  "/ptlist?period=1h&tz=Europe/Athens&t1=20180214T204603&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"unsupported time format",
				},
			},
		},
		{
			name: "Invalid timestamp",
			url:  "/ptlist?period=1h&tz=Europe/Ath&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"invalid timezone",
				},
			},
		},
		{
			name: "Invalid timestamp",
			url:  "/ptlist?period=h&tz=Europe/Athens&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"invalid period format",
				},
			},
		},
		{
			name: "missing period request parameter",
			url:  "/ptlist?tz=Europe/Athens&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"parameter `period` missing",
				},
			},
		},
		{
			name: "missing timezone request parameter",
			url:  "/ptlist?period=5s&t1=20180214T204603Z&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"parameter `tz - Timezone` missing",
				},
			},
		},
		{
			name: "missing t1 request parameter",
			url:  "/ptlist?period=5s&tz=Europe/Athens&t2=20211115T123456Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"parameter `t1 - Start date` missing",
				},
			},
		},
		{
			name: "missing t2 request parameter",
			url:  "/ptlist?period=5s&tz=Europe/Athens&t1=20180214T204603Z",
			want: response{
				status: http.StatusBadRequest,
				body: []string{
					"parameter `t2 - End date` missing",
				},
			},
		},
	}
	cl := &http.Client{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, _ := http.NewRequest("GET", ts.URL+tt.url, nil)
			resp, err := cl.Do(req)
			if err != nil {
				t.Errorf("error executing REST call %s", err.Error())
			}
			if resp.StatusCode != tt.want.status {
				t.Errorf("unexpected status code returned %d want %d", resp.StatusCode, tt.want.status)
			}
			if tt.want.status != http.StatusOK {
				body, _ := io.ReadAll(resp.Body)
				var reason errorResponse
				json.Unmarshal(body, &reason)
				if !reflect.DeepEqual(reason.Description, tt.want.body[0]) {
					t.Errorf("invalid response got %s want %s", reason, tt.want.body[0])
				}
			} else {
				timestamps := make([]string, 0)
				body, _ := io.ReadAll(resp.Body)
				err = json.Unmarshal(body, &timestamps)
				if err != nil {
					t.Errorf("could not deserialize body to []string")
				}
				if !reflect.DeepEqual(timestamps, tt.want.body) {
					t.Errorf("invalid response got %s want %s", timestamps, tt.want.body)
				}
			}

		})
	}
}
