package rest

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"task_ticker/internal/core"
	"task_ticker/internal/core/service"
	"task_ticker/pkg/utils"
)

type Server struct {
	Port int
	svc  service.Ticker
}

type errorResponse struct {
	Status      string `json:"status"`
	Description string `json:"desc"`
}

func (s *Server) Start() {
	s.svc = &service.TickerService{}
	err := http.ListenAndServe(":"+strconv.Itoa(s.Port), s.RegisterRouterEndpoints())
	if err != nil {
		log.Fatal(err)
	}
}

func (s *Server) interval(w http.ResponseWriter, r *http.Request) {
	criteria, err := parseRequest(r.URL.Query())
	w.Header().Add("content-type", "application/json")
	if err != nil {
		sendErrorResponse("error", err.Error(), w)
		return
	}

	timestamps := s.svc.Tick(criteria)

	resp, err := json.Marshal(timestamps)
	if err != nil {
		log.Printf("error serializing resposne %v, %s \n", resp, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	sendResponse(http.StatusOK, resp, w)
}

func parseRequest(params url.Values) (core.Criteria, error) {
	period := params.Get("period")
	timezone := params.Get("tz")
	dateFrom := params.Get("t1")
	dateTo := params.Get("t2")

	interval, err := utils.ParseInterval(period, core.SupportedTypes)
	if err != nil {
		return core.Criteria{}, err
	}

	location, err := utils.ParseLocation(timezone)
	if err != nil {
		return core.Criteria{}, err
	}
	timeDateFrom, err := utils.ParseTimestamp(dateFrom, time.UTC)
	if err != nil {
		return core.Criteria{}, err
	}
	timeDateTo, err := utils.ParseTimestamp(dateTo, time.UTC)
	if err != nil {
		return core.Criteria{}, err
	}

	return core.Criteria{
		Timezone: *location,
		DateFrom: timeDateFrom,
		DateTo:   timeDateTo,
		Period:   interval,
	}, nil
}
