package service

import (
	"time"

	"task_ticker/internal/core"
	"task_ticker/pkg/types"
	"task_ticker/pkg/utils"
)

type Ticker interface {
	Tick(criteria core.Criteria) []string
}

type TickerService struct {
}

func (t TickerService) Tick(c core.Criteria) []string {
	duration := utils.ParseDuration(c.Period)
	timestamps := make([]string, 0)
	invocationTime := invocationPoint(c)
	for {
		invocationTime = duration.ShiftTime(invocationTime)
		if invocationTime.After(c.DateTo) || invocationTime.Equal(c.DateTo) {
			break
		}
		timestamps = append(timestamps, utils.FormatTime(invocationTime, time.UTC))
	}
	return timestamps
}

// invocationPoint Return the start of the task period depending on the core.Criteria period type input
func invocationPoint(c core.Criteria) time.Time {
	y, m, d := c.DateFrom.In(&c.Timezone).Date()
	th, tm, ts := c.DateFrom.In(&c.Timezone).Clock()
	switch c.Period.Type {
	case types.BySecond:
		return time.Date(y, m, d, th, tm, ts, 0, &c.Timezone)
	case types.ByMinute:
		return time.Date(y, m, d, th, tm, 0, 0, &c.Timezone)
	case types.ByHour:
		return time.Date(y, m, d, th, 0, 0, 0, &c.Timezone)
	case types.ByDay:
		return time.Date(y, m, d, 0, 0, 0, 0, &c.Timezone)
	case types.ByMonth:
		return time.Date(y, m, 1, 0, 0, 0, 0, &c.Timezone)
	case types.ByYear:
		return time.Date(y, 1, 1, 0, 0, 0, 0, &c.Timezone)
	default:
		return time.Time{}
	}
}
