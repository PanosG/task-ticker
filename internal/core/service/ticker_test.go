package service

import (
	"reflect"
	"testing"
	"time"

	"task_ticker/internal/core"
	"task_ticker/pkg/utils"
)

// Based on the examples provided
func TestTickerService_Tick(t1 *testing.T) {
	loc, _ := time.LoadLocation("Europe/Athens")
	type args struct {
		c core.Criteria
	}
	var tests = []struct {
		name     string
		criteria core.Criteria
		want     []string
	}{
		{
			name: "Hourly Example Case",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2021, 07, 14, 20, 46, 03, 0, time.UTC),
				DateTo:   time.Date(2021, 07, 15, 12, 34, 56, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 1,
					Type:      "h",
				},
			},
			want: []string{"20210714T210000Z",
				"20210714T220000Z",
				"20210714T230000Z",
				"20210715T000000Z",
				"20210715T010000Z",
				"20210715T020000Z",
				"20210715T030000Z",
				"20210715T040000Z",
				"20210715T050000Z",
				"20210715T060000Z",
				"20210715T070000Z",
				"20210715T080000Z",
				"20210715T090000Z",
				"20210715T100000Z",
				"20210715T110000Z",
				"20210715T120000Z"},
		},
		{
			name: "Daily Example",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2021, 10, 10, 20, 46, 03, 0, time.UTC),
				DateTo:   time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 1,
					Type:      "d",
				},
			},
			want: []string{"20211010T210000Z",
				"20211011T210000Z",
				"20211012T210000Z",
				"20211013T210000Z",
				"20211014T210000Z",
				"20211015T210000Z",
				"20211016T210000Z",
				"20211017T210000Z",
				"20211018T210000Z",
				"20211019T210000Z",
				"20211020T210000Z",
				"20211021T210000Z",
				"20211022T210000Z",
				"20211023T210000Z",
				"20211024T210000Z",
				"20211025T210000Z",
				"20211026T210000Z",
				"20211027T210000Z",
				"20211028T210000Z",
				"20211029T210000Z",
				"20211030T210000Z",
				"20211031T220000Z",
				"20211101T220000Z",
				"20211102T220000Z",
				"20211103T220000Z",
				"20211104T220000Z",
				"20211105T220000Z",
				"20211106T220000Z",
				"20211107T220000Z",
				"20211108T220000Z",
				"20211109T220000Z",
				"20211110T220000Z",
				"20211111T220000Z",
				"20211112T220000Z",
				"20211113T220000Z",
				"20211114T220000Z"},
		},
		{
			name: "Monthly Example",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2021, 02, 14, 20, 46, 03, 0, time.UTC),
				DateTo:   time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 1,
					Type:      "mo",
				},
			},
			want: []string{"20210228T220000Z",
				"20210331T210000Z",
				"20210430T210000Z",
				"20210531T210000Z",
				"20210630T210000Z",
				"20210731T210000Z",
				"20210831T210000Z",
				"20210930T210000Z",
				"20211031T220000Z"},
		},
		{
			name: "Yearly Example",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2018, 02, 14, 20, 46, 03, 0, time.UTC),
				DateTo:   time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 1,
					Type:      "y",
				},
			},
			want: []string{"20181231T220000Z",
				"20191231T220000Z",
				"20201231T220000Z"},
		},
		{
			name: "By Minute",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2021, 02, 14, 20, 46, 03, 0, time.UTC),
				DateTo:   time.Date(2021, 02, 14, 21, 18, 56, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 4,
					Type:      "m",
				},
			},
			want: []string{"20210214T205000Z",
				"20210214T205400Z",
				"20210214T205800Z",
				"20210214T210200Z",
				"20210214T210600Z",
				"20210214T211000Z",
				"20210214T211400Z",
				"20210214T211800Z"},
		},
		{
			name: "Seconds Example",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: time.Date(2021, 02, 14, 21, 46, 05, 0, time.UTC),
				DateTo:   time.Date(2021, 02, 14, 21, 46, 50, 0, time.UTC),
				Period: utils.Interval{
					Frequency: 5,
					Type:      "s",
				},
			},
			want: []string{"20210214T214610Z",
				"20210214T214615Z",
				"20210214T214620Z",
				"20210214T214625Z",
				"20210214T214630Z",
				"20210214T214635Z",
				"20210214T214640Z",
				"20210214T214645Z"},
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := TickerService{}
			if got := t.Tick(tt.criteria); !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("Tick() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_invocationPoint(t *testing.T) {
	_time := time.Now()
	loc, _ := time.LoadLocation("Europe/Athens")
	tests := []struct {
		name     string
		criteria core.Criteria
		want     time.Time
	}{
		{
			name: "Year rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "y",
				},
			},
			want: time.Date(_time.Year(), 1, 1, 0, 0, 0, 0, loc),
		},
		{
			name: "Month rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "mo",
				},
			},
			want: time.Date(_time.Year(), _time.Month(), 1, 0, 0, 0, 0, loc),
		},
		{
			name: "Day rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "d",
				},
			},
			want: time.Date(_time.Year(), _time.Month(), _time.Day(), 0, 0, 0, 0, loc),
		},
		{
			name: "Hour rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "h",
				},
			},
			want: time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), 0, 0, 0, loc),
		},
		{
			name: "Minute rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "m",
				},
			},
			want: time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), _time.Minute(), 0, 0, loc),
		},
		{
			name: "Minute rounding",
			criteria: core.Criteria{
				Timezone: *loc,
				DateFrom: _time,
				Period: utils.Interval{
					Frequency: 1,
					Type:      "s",
				},
			},
			want: time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), _time.Minute(), _time.Second(), 0, loc),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := invocationPoint(tt.criteria); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("invocationPoint() = %v, want %v", got, tt.want)
			}
		})
	}
}
