package core

import (
	"time"

	"task_ticker/pkg/types"
	"task_ticker/pkg/utils"
)

var SupportedTypes = []string{types.ByHour, types.ByDay, types.ByMonth, types.ByYear, types.ByMinute, types.BySecond}

type Criteria struct {
	Timezone time.Location
	DateFrom time.Time
	DateTo   time.Time
	Period   utils.Interval
}
