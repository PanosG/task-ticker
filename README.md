# Period Ticker Service
## Supported Periods

To support multiple kind of period inputs an ISO8601 duration like structure is being used internal by the application, [duration](pkg/utils/duration.go), supporting
only _integer_ inputs for simplicity. Again, for simplicity the input periods are as in the assignment (h,d,mo,y), with the addition of _s_ for seconds and _m_ for minutes.

The application supported periods can be modified in the [criteria](internal/core/criteria.go) _SupportedTypes_. In order to add extra period types, the shorthand must be defined in
[types](pkg/types/types.go), along with a way to transform this value into the ISO8601 format [duration](pkg/utils/duration.go) _ParseDuration()_, finally it should be added in the _SupportedTypes_.

The ISO8601 duration standard format could be used in the future as the REST _period_ parameter giving more flexibility on the service.

## Run

### Standalone
You can run the application by utilizing the `make run` command. Default port for the service is 8080, but can be overridden with the _PORT_ argument .
e.g `make run PORT=9000`, the exposed endpoint is the same as the examples provided.
 
### Docker
To run the application in docker use _make docker-run_, specifying the image version(_VERSION_), the application port (_PORT_)
along with the docker exposed port(_EXP_PORT_). e.g `make docker-run VERSION=0.0.1 EXPOSED_PORT=8080 PORT=9000`.

If no values are specified, defaults are applied as VERSION=latest, PORT=8080, EXP_PORT=8080

### Tests

There are also _make_ goals for tests, `make test` and `make cover`. The test cases present in [ticker_test](internal/core/service/ticker_test.go) and [server_test](internal/rest/server_test.go)
contain example cases included in the assignment document along with others regarding input validation and extended cases.

