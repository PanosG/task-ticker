ARG OARCH
ARG CGO_FLAG=0
ARG OARCH=amd64
ARG GOOS=linux

FROM golang:alpine AS builder
WORKDIR /build
ADD go.mod .
COPY . .
RUN CGO_ENABLED=${CGO_FLAG} OARCH=${OARCH} GOOS=${GOOS} go build -o app ./cmd/main.go

FROM alpine
RUN apk update && apk --no-cache add tzdata
WORKDIR /app
COPY --from=builder /build/app /app
CMD ./app -p ${PORT}