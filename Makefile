CGO_FLAG=0
OARCH=amd64
GOOS=linux
PORT=8080
EXP_PORT=8080
VERSION=latest

build:
	CGO_ENABLED=${CGO_FLAG} OARCH=${OARCH} GOOS=${GOOS} go build -o app ./cmd/main.go

docker:
	docker build --build-arg CGO_FLAG=${CGO_FLAG} --build-arg GOOS=${GOOS} --build-arg OARCH=${OARCH} -t task_ticker:${VERSION} .

docker-run:
	${MAKE} docker
	docker run --env PORT=${PORT} -p${EXP_PORT}:${PORT} task_ticker:${VERSION}


run:
	${MAKE} build
	./app -p ${PORT}

test:
	go test ./...

cover:
	go test --cover ./...