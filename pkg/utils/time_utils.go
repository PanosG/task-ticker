package utils

import (
	"log"
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const timestampFormat = "20060102T150405Z"

var intervalRegex = regexp.MustCompile("(^[1-9][0-9]*)([a-z]+)")
var invalidTZError = errors.New("invalid timezone")
var invalidTimeFormatError = errors.New("unsupported time format")
var invalidPeriodFormatError = errors.New("invalid period format")
var unsupportedPeriodTypeError = errors.New("unsupported period")

// Interval describes the period argument in the queries
type Interval struct {
	Frequency int
	Type      string
}

// ParseInterval transform period input to Interval
func ParseInterval(period string, supportedTypes []string) (Interval, error) {
	if !intervalRegex.MatchString(period) {
		return Interval{}, invalidPeriodFormatError
	}
	periodParts := intervalRegex.FindStringSubmatch(period)

	if !isSupported(periodParts[2], supportedTypes) {
		return Interval{}, unsupportedPeriodTypeError
	}
	interval := periodParts[1]

	freq, _ := strconv.Atoi(interval)
	return Interval{
		Frequency: freq,
		Type:      periodParts[2],
	}, nil
}

func ParseTimestamp(timestamp string, timezone *time.Location) (time.Time, error) {
	parsedTime, err := time.ParseInLocation(timestampFormat, timestamp, timezone)
	if err != nil {
		return time.Time{}, invalidTimeFormatError
	}
	return parsedTime, nil
}

func FormatTime(t time.Time, loc *time.Location) string {
	return t.In(loc).Format(timestampFormat)
}

func isSupported(periodType string, supportedTypes []string) bool {
	for _, t := range supportedTypes {
		if periodType == t {
			return true
		}
	}
	return false
}

func ParseLocation(timezone string) (*time.Location, error) {
	location, err := time.LoadLocation(timezone)
	if err != nil {
		log.Println(err)
		return nil, invalidTZError
	}
	return location, nil
}
