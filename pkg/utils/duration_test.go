package utils

import (
	"reflect"
	"testing"
	"time"

	"task_ticker/pkg/types"
)

func TestDuration_ShiftTime(t *testing.T) {
	startTime := time.Date(2022, 02, 25, 15, 34, 40, 0, time.UTC)
	tests := []struct {
		name     string
		duration *Duration
		start    time.Time
		want     time.Time
	}{
		{
			name:     "Shift by 1 Hour",
			duration: &Duration{TH: 1},
			start:    startTime,
			want:     startTime.Add(time.Hour * 1),
		},
		{
			name:     "Shift by 1 Day",
			duration: &Duration{D: 1},
			start:    startTime,
			want:     startTime.AddDate(0, 0, 1),
		},
		{
			name:     "Shift by 1 Month",
			duration: &Duration{M: 1},
			start:    startTime,
			want:     startTime.AddDate(0, 1, 0),
		},
		{
			name:     "Shift by 1 Year",
			duration: &Duration{Y: 1},
			start:    startTime,
			want:     startTime.AddDate(1, 0, 0),
		},
		{
			name:     "Shift by 1 Year, 1 Month, 2 Days",
			duration: &Duration{Y: 1, M: 1, D: 2},
			start:    startTime,
			want:     startTime.AddDate(1, 1, 2),
		},
		{
			name:     "Shift by 2 Days and 10 Hours",
			duration: &Duration{D: 2, TH: 10},
			start:    startTime,
			want:     startTime.AddDate(0, 0, 2).Add(time.Hour * 10),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.duration.ShiftTime(tt.start); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ShiftTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_String(t *testing.T) {
	tests := []struct {
		name string
		arg  Duration
		want string
	}{
		{
			name: "Should not contain date part",
			arg: Duration{
				TH: 12,
				TM: 5,
				TS: 25,
			},
			want: "PT12H5M25S",
		},
		{
			name: "Should not contain time part",
			arg: Duration{
				Y: 12,
				M: 5,
				D: 25,
			},
			want: "P12Y5M25D",
		},
		{
			name: "Should contain both parts",
			arg: Duration{
				Y:  12,
				M:  5,
				D:  25,
				TH: 10,
				TM: 11,
				TS: 3,
			},
			want: "P12Y5M25DT10H11M3S",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.arg.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_buildDatePart(t *testing.T) {
	tests := []struct {
		name     string
		duration *Duration
		arg      string
		want     string
	}{
		{
			name:     "Date part",
			duration: &Duration{D: 10},
			arg:      "P",
			want:     "P10D",
		},
		{
			name:     "Year part",
			duration: &Duration{Y: 2},
			arg:      "P",
			want:     "P2Y",
		},
		{
			name:     "Month part",
			duration: &Duration{M: 5},
			arg:      "P",
			want:     "P5M",
		},
		{
			name:     "Concatenated part",
			duration: &Duration{Y: 2, M: 5, D: 13},
			arg:      "P",
			want:     "P2Y5M13D",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.duration.buildDatePart(tt.arg); got != tt.want {
				t.Errorf("buildDatePart() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_buildTimePart(t *testing.T) {
	tests := []struct {
		name     string
		duration *Duration
		arg      string
		want     string
	}{
		{
			name:     "Hour part",
			duration: &Duration{TH: 5},
			arg:      "P",
			want:     "PT5H",
		},
		{
			name:     "Minute part",
			duration: &Duration{TM: 35},
			arg:      "P",
			want:     "PT35M",
		},
		{
			name:     "Seconds part",
			duration: &Duration{TS: 33},
			arg:      "P",
			want:     "PT33S",
		},
		{
			name:     "Concatenated part",
			duration: &Duration{TH: 5, TM: 35, TS: 33},
			arg:      "P",
			want:     "PT5H35M33S",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.duration.buildTimePart(tt.arg); got != tt.want {
				t.Errorf("buildTimePart() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_hasDatePart(t *testing.T) {
	tests := []struct {
		name     string
		duration *Duration
		want     bool
	}{
		{
			name: "Should return true",
			duration: &Duration{
				Y:  0,
				M:  5,
				D:  0,
				TH: 1,
				TM: 2,
				TS: 3,
			},
			want: true,
		},
		{
			name: "Should return false",
			duration: &Duration{
				Y:  0,
				M:  0,
				D:  0,
				TH: 2,
				TM: 5,
				TS: 3,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.duration.hasDatePart(); got != tt.want {
				t.Errorf("hasDatePart() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_hasTimePart(t *testing.T) {
	tests := []struct {
		name     string
		duration *Duration
		want     bool
	}{
		{
			name: "Should return true",
			duration: &Duration{
				Y:  0,
				M:  5,
				D:  0,
				TH: 1,
				TM: 2,
				TS: 3,
			},
			want: true,
		},
		{
			name: "Should return false",
			duration: &Duration{
				Y:  0,
				M:  5,
				D:  0,
				TH: 0,
				TM: 0,
				TS: 0,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.duration.hasTimePart(); got != tt.want {
				t.Errorf("hasTimePart() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDuration_ParseDuration(t *testing.T) {
	tests := []struct {
		name     string
		interval Interval
		want     *Duration
	}{
		{
			name: "Should split hours into days + hours",
			interval: Interval{
				Type:      types.ByHour,
				Frequency: 49,
			},
			want: &Duration{D: 2, TH: 1},
		},
		{
			name: "Should split months into years + months",
			interval: Interval{
				Type:      types.ByMonth,
				Frequency: 14,
			},
			want: &Duration{Y: 1, M: 2},
		},
		{
			name: "Should contain only hours",
			interval: Interval{
				Type:      types.ByHour,
				Frequency: 5,
			},
			want: &Duration{TH: 5},
		},
		{
			name: "Should contain only months",
			interval: Interval{
				Type:      types.ByMonth,
				Frequency: 5,
			},
			want: &Duration{M: 5},
		},
		{
			name: "Should contain only years",
			interval: Interval{
				Type:      types.ByYear,
				Frequency: 2,
			},
			want: &Duration{Y: 2},
		},
		{
			name: "Should contain only days",
			interval: Interval{
				Type:      types.ByDay,
				Frequency: 5,
			},
			want: &Duration{D: 5},
		},
		{
			name: "Should split minutes into Hours and Minutes",
			interval: Interval{
				Type:      types.ByMinute,
				Frequency: 135,
			},
			want: &Duration{TH: 2, TM: 15},
		},
		{
			name: "Should contain only minutes",
			interval: Interval{
				Type:      types.ByMinute,
				Frequency: 24,
			},
			want: &Duration{TM: 24},
		},
		{
			name: "Should split seconds into Minutes and Seconds",
			interval: Interval{
				Type:      types.BySecond,
				Frequency: 135,
			},
			want: &Duration{TM: 2, TS: 15},
		},
		{
			name: "Should contain only seconds",
			interval: Interval{
				Type:      types.BySecond,
				Frequency: 24,
			},
			want: &Duration{TS: 24},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseDuration(tt.interval); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}
