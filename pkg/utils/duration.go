package utils

import (
	"fmt"
	"math"
	"time"

	"task_ticker/pkg/types"
)

// Duration Utilize ISO8601-like duration formatting to provide extensible support for time shifting.
// For simplicity, we allow only int values.
// Minutes(TM) and Seconds (TS) are not allowed in the input, but can be parsed in the future.
type Duration struct {
	Y  int
	M  int
	D  int
	TH int
	TM int
	TS int
}

// ParseDuration transforms the period input to ISO8601 Duration
func ParseDuration(interval Interval) *Duration {
	switch interval.Type {
	case types.BySecond:
		if interval.Frequency >= 60 {
			minutes := int(math.Floor(float64(interval.Frequency / 60)))
			seconds := int(math.Floor(float64(interval.Frequency % 60)))
			return &Duration{TM: minutes, TS: seconds}
		}
		return &Duration{TS: interval.Frequency}
	case types.ByMinute:
		if interval.Frequency >= 60 {
			hours := int(math.Floor(float64(interval.Frequency / 60)))
			minutes := int(math.Floor(float64(interval.Frequency % 60)))
			return &Duration{TM: minutes, TH: hours}
		}
		return &Duration{TM: interval.Frequency}
	case types.ByHour:
		if interval.Frequency >= 24 {
			days := int(math.Floor(float64(interval.Frequency / 24)))
			hours := int(math.Floor(float64(interval.Frequency % 24)))
			return &Duration{D: days, TH: hours}
		}
		return &Duration{TH: interval.Frequency}
	case types.ByDay:
		return &Duration{D: interval.Frequency}
	case types.ByMonth:
		if interval.Frequency >= 12 {
			years := int(math.Floor(float64(interval.Frequency / 12)))
			months := int(math.Floor(float64(interval.Frequency % 12)))
			return &Duration{Y: years, M: months}
		}
		return &Duration{M: interval.Frequency}
	case types.ByYear:
		return &Duration{Y: interval.Frequency}
	default:
		return nil
	}
}

// ShiftTime apply the
func (d Duration) ShiftTime(t time.Time) time.Time {
	t = t.AddDate(d.Y, d.M, d.D)
	var duration time.Duration
	duration += time.Hour * time.Duration(d.TH)
	duration += time.Minute * time.Duration(d.TM)
	duration += time.Second * time.Duration(d.TS)
	return t.Add(duration)
}

func (d Duration) String() string {
	s := "P"
	if d.hasDatePart() {
		s = d.buildDatePart(s)
	}
	if d.hasTimePart() {
		s = d.buildTimePart(s)
	}
	return s
}

func (d Duration) hasDatePart() bool {
	return d.Y != 0 || d.M != 0 || d.D != 0
}

func (d Duration) hasTimePart() bool {
	return d.TH != 0 || d.TM != 0 || d.TS != 0
}

func (d Duration) buildDatePart(s string) string {
	if d.Y != 0 {
		s = fmt.Sprintf("%s%dY", s, d.Y)
	}
	if d.M != 0 {
		s = fmt.Sprintf("%s%dM", s, d.M)
	}
	if d.D != 0 {
		s = fmt.Sprintf("%s%dD", s, d.D)
	}
	return s
}

func (d Duration) buildTimePart(s string) string {
	s += "T"
	if d.TH != 0 {
		s = fmt.Sprintf("%s%dH", s, d.TH)
	}
	if d.TM != 0 {
		s = fmt.Sprintf("%s%dM", s, d.TM)
	}
	if d.TS != 0 {
		s = fmt.Sprintf("%s%dS", s, d.TS)
	}
	return s
}
