package utils

import (
	"reflect"
	"testing"
	"time"

	"task_ticker/pkg/types"
)

var supportedTypes = []string{"mo", "h", "d", "m", "y", "s"}

func TestParseTimestamp(t *testing.T) {
	type args struct {
		timestamp string
		timezone  string
	}
	tests := []struct {
		name    string
		args    args
		want    time.Time
		wantErr error
	}{
		{
			name:    "Valid input",
			args:    args{timezone: "UTC", timestamp: "20221202T204603Z"},
			want:    time.Date(2022, 12, 02, 20, 46, 03, 0, time.UTC),
			wantErr: nil,
		},
		{
			name:    "Invalid time format",
			args:    args{timezone: "UTC", timestamp: "20221202-204603Z"},
			want:    time.Time{},
			wantErr: invalidTimeFormatError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tz, _ := time.LoadLocation(tt.args.timezone)
			got, err := ParseTimestamp(tt.args.timestamp, tz)
			if tt.wantErr != nil && err != tt.wantErr {
				t.Errorf("ParseTimestamp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseTimestamp() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseLocation(t *testing.T) {
	tests := []struct {
		name     string
		timezone string
		wantErr  error
	}{
		{
			name:     "Should parse Europe/Athens",
			timezone: "Europe/Athens",
			wantErr:  nil,
		},
		{
			name:     "Should parse UTC",
			timezone: "UTC",
			wantErr:  nil,
		},
		{
			name:     "Invalid timezone",
			timezone: "TCU",
			wantErr:  invalidTZError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseLocation(tt.timezone)
			if tt.wantErr != nil && err != tt.wantErr {
				t.Errorf("ParseTimestamp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr == nil && got.String() != tt.timezone {
				t.Errorf("ParseTimestamp() got = %v, want %v", got, tt.timezone)
			}
		})
	}
}

func TestParseInterval(t *testing.T) {

	type args struct {
		period         string
		supportedTypes []string
	}
	tests := []struct {
		name    string
		period  string
		want    Interval
		wantErr error
	}{
		{
			name:   "Valid Month Input",
			period: "1mo",
			want: Interval{
				Frequency: 1,
				Type:      types.ByMonth,
			},
			wantErr: nil,
		},
		{
			name:   "Valid Day Input",
			period: "1d",
			want: Interval{
				Frequency: 1,
				Type:      types.ByDay,
			},
			wantErr: nil,
		},
		{
			name:   "Valid Year Input",
			period: "1y",
			want: Interval{
				Frequency: 1,
				Type:      types.ByYear,
			},
			wantErr: nil,
		},
		{
			name:   "Valid minute Input",
			period: "5m",
			want: Interval{
				Frequency: 5,
				Type:      types.ByMinute,
			},
			wantErr: nil,
		},
		{
			name:   "Valid Seconds Input",
			period: "25s",
			want: Interval{
				Frequency: 25,
				Type:      types.BySecond,
			},
			wantErr: nil,
		},
		{
			name:    "Invalid Period - Starts with 0",
			period:  "0mo",
			want:    Interval{},
			wantErr: invalidPeriodFormatError,
		},
		{
			name:    "Invalid Period - Unsupported type",
			period:  "1dt",
			want:    Interval{},
			wantErr: unsupportedPeriodTypeError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseInterval(tt.period, supportedTypes)
			if tt.wantErr != nil && err != tt.wantErr {
				t.Errorf("ParseTimestamp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr == nil && got != tt.want {
				t.Errorf("ParseTimestamp() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFormatTime(t *testing.T) {
	type args struct {
		t        time.Time
		timezone string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "UTC Format",
			args: args{
				t:        time.Date(2022, 12, 3, 20, 15, 22, 0, time.UTC),
				timezone: "UTC",
			},
			want: "20221203T201522Z",
		},
		{
			name: "EU/Athens Format",
			args: args{
				t:        time.Date(2022, 12, 3, 20, 15, 22, 0, time.UTC),
				timezone: "Europe/Athens",
			},
			want: "20221203T221522Z",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			loc, _ := ParseLocation(tt.args.timezone)
			if got := FormatTime(tt.args.t, loc); got != tt.want {
				t.Errorf("FormatTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isSupported(t *testing.T) {
	tests := []struct {
		name       string
		periodType string
		want       bool
	}{
		{
			name:       "Seconds",
			periodType: "s",
			want:       true,
		},
		{
			name:       "Hours",
			periodType: "h",
			want:       true,
		},
		{
			name:       "Days",
			periodType: "d",
			want:       true,
		},
		{
			name:       "Minutes",
			periodType: "m",
			want:       true,
		},
		{
			name:       "Years",
			periodType: "y",
			want:       true,
		},
		{
			name:       "Weeks",
			periodType: "w",
			want:       false,
		},
		{
			name:       "Nanos",
			periodType: "ns",
			want:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isSupported(tt.periodType, supportedTypes); got != tt.want {
				t.Errorf("isSupported() = %v, want %v", got, tt.want)
			}
		})
	}
}
