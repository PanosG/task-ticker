package types

const (
	BySecond = "s"
	ByMinute = "m"
	ByHour   = "h"
	ByDay    = "d"
	ByMonth  = "mo"
	ByYear   = "y"
)
