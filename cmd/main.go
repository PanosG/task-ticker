package main

import (
	"flag"
	"log"
	"strconv"

	"task_ticker/internal/rest"
)

func main() {

	flagPort := flag.String("p", "", "HTTP server port")
	flag.Parse()
	if *flagPort == "" {
		log.Fatal("HTTP server port param must be specified `-p`")
	}
	srvPort, err := strconv.Atoi(*flagPort)
	if err != nil || srvPort <= 1024 {
		log.Fatal("invalid port")
	}

	log.Printf("Starting HTTP server on port %d\n", srvPort)

	srv := &rest.Server{Port: srvPort}
	srv.Start()
}
